'use strict';
const gulp = require('gulp'),
    sass = require('gulp-sass');

gulp.task('sass', function () {
    return gulp.src('./scss/*.scss')
        .pipe(sass({includePaths: ['./node_modules/susy/sass']}))
        .pipe(gulp.dest('./css/'));
});